Attribute VB_Name = "Modulo_Agente_Trans"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpString _
As Any, ByVal lpFileName As String) As Long
    
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Type Estruc_Config
    
    ArchivoInicio                                       As String
    Srv_Origen                                          As String
    Srv_Destino                                         As String
    BD_Origen                                           As String
    BD_Destino                                          As String
    BD_POS_Origen                                       As String
    BD_POS_Destino                                      As String
    User_Origen                                         As String
    User_Destino                                        As String
    Pwd_Origen                                          As String
    Pwd_Destino                                         As String
    Loc_Origen                                          As String
    Loc_Destino                                         As String
    RutaHistoricos                                      As String
    RutaDestino                                         As String
    RutaNoProcesados                                    As String
    RutaCorrelativosValidos                             As String
    
    IdMACompra                                          As String
    IDTrCompra                                          As String
    IdMACompraEspera                                    As String
    IDTrCompraEspera                                    As String
    
    IdMaVentas                                          As String
    IdTrVentas                                          As String
    IdMaInvent                                          As String
    IdTrInvent                                          As String
    
    IdMaBancos                                          As String
    IdDepoProd                                          As String
    IdMaClientes                                        As String
    IdDocumentoFiscal                                   As String
    
    IdCxpCompra                                         As String
    IdCxpCompraImp                                      As String
    IdCxcVenta                                          As String
    IdCxcVentaImp                                       As String
    
    IdCompraImp                                         As String
    IdVentaImp                                          As String
    
    IdVentasPos                                         As String ' MA_PAGOS
    IdVentasPosImp                                      As String ' MA_PAGOS_IMPUESTOS
    IdVentasPosDFI                                      As String ' MA_DOCUMENTOS_FISCAL (ORIGEN POS)
    IdVentasPosDP                                       As String ' MA_DETALLEPAGO
    IdVentasPosDTR                                      As String ' MA_TRANSACCION
    IdVentasPosDVLT                                     As String ' MA_DETALLEPAGO_VUELTO
    IdVentasPOS_DN                                      As String ' MA_DONACIONES
    IdVentasPOS_DN_DP                                   As String ' MA_DONACIONES_DETPAG
    IdVentasPOS_PxE                                     As String ' MA_TRANSACCION_PENDIENTE_X_ENTREGA
    IdVentasPOS_PPxE                                    As String ' MA_TRANSACCION_PLAN_PENDIENTE_X_ENTREGA
    IdVentasPOS_Ser                                     As String ' MA_TRANSACCION_SERIALES
    IdVentasPOS_Cup                                     As String ' MA_TRANSACCION_CUPONES
    IdVentasPOS_DetPrm                                  As String ' MA_TRANSACCION_DETALLE_PROMO
    IDVentasPOS_Pend_Ctrl_Dev                           As String ' TR_PEND_MA_TRANSACCION_CTRL_DEV
    
    IdVentasFOOD                                        As String ' MA_VENTAS_POS
    IdVentasFOOD_Imp                                    As String ' MA_VENTAS_POS_IMPUESTOS
    IdVentasFOOD_DFI                                    As String ' MA_DOCUMENTOS_FISCAL (ORIGEN FOOD)
    IdVentasFOOD_DP                                     As String ' TR_VENTAS_POS_PAGOS
    IdVentasFOOD_DTR                                    As String ' TR_VENTAS_POS
    IdVentasFOOD_DetImp                                 As String ' TR_VENTAS_POS_IMPUESTOS
    IdVentasFOOD_DVLT                                   As String ' TABLA DETALLE VUELTO FOOD (AUN NO DISPONIBLE)
    
    IdVentasAvanceEfectivo                              As String ' MA_AVANCE_EFECTIVO
    IdVentasAvanceEfectivo_DP                           As String ' MA_AVANCE_EFECTIVO_DETPAG
    
    IdCierresPOS_MA                                     As String ' MA_CIERRES
    IdCierresPOS_TR                                     As String ' TR_CIERRES
    IdCierresPOS_Fondo_MA                               As String ' MA_CIERRES_FONDO
    'IdCierresPOS_Fondo_TR                               As String ' TR_CIERRES_FONDO
    
    IdSolTra_MA                                         As String ' MA_REQUISICIONES
    IdSolTra_TR                                         As String ' TR_REQUISICIONES
    IdSolTra_REL                                        As String ' MA_RELACION_SOLICITUD_TRANSFERENCIA
    
    IdSolCom_MA                                         As String ' MA_SOLICITUDCOMPRAS
    IdSolCom_TR                                         As String ' TR_SOLICITUDCOMPRAS
    
    EsOrigen                                            As Boolean
    EsDestino                                           As Boolean
    
    EsInterfazCxP                                       As Integer
    EsInterfazCxP_PorLocalidad                          As Boolean
    EsInterfazCxC                                       As Boolean
    
    EnviarSolicitudesTransferencia                      As Boolean
    CompletarOrigenSolicitudTransferencia               As Boolean
    
    EnviarSolicitudesDeCompra                           As Boolean
    CompletarOrigenSolicitudCompra                      As Boolean
    
    InterfazCxC_NoCancelarOrigen                        As Boolean
    InterfazCxC_Cruces                                  As Boolean
    InterfazCxC_NoSubirAnticiposPendientes              As Boolean
    
    ManejaClienteFrecuente                              As Boolean
    Acumular_X_Rif                                      As Boolean
    SoloClientes                                        As Boolean
    
    MostrarDebug                                        As Boolean
    
    SucursalNoCompletaRec                               As Boolean
    SucursalNoCompletaRec_TiempoMaximoMinutos           As Long
    
    SucursalNoCompletaNdc                               As Boolean
    SucursalNoCompletaNdc_TiempoMaximoMinutos           As Long
    
    LongitudSucursal                                    As Integer
    ValorPunto                                          As Double
    PuntosRetailCampoBase                               As String
    
    TransferirClientesWebCentral                        As Boolean ' Subir Clientes Web a Corporativo
    TransferirDireccionesWebCentral                     As Boolean ' Subir Direcciones de Clientes Web
    
    TransferirDetallesPOSRetail                         As Boolean ' Detalle y Detalle de Pago
    TransferirDonacionesPOSRetail                       As Boolean ' Donaciones
    TransferirDetalleVueltoPOSRetail                    As Boolean ' Detalle de Vuelto
    TransferirPendXEntregaPOSRetail                     As Boolean ' Pendientes por Entrega
    TransferirSerialesTransaccionPOSRetail              As Boolean ' Seriales
    TransferirCuponesTransaccionPOSRetail               As Boolean ' Cupones
    TransferirDetallePromoPOSRetail                     As Boolean ' Detalle Promo
    
    ' 2021-08-23 ' Nuevas por que no exist�an. En este caso se crea una variable _
    para transferir las ventas food para que el cliente que actualice el agente _
    no se vea afectado, debido a que podr�a tener gran cantidad de transacciones _
    acumuladas y esto podr�a originar unos MEGA ADTGs super pesados que tranquen _
    la sincronizaci�n. Por este motivo no lo haremos obligatorio, sino que transferir _
    las ventas food sea de manera controlada. En este caso si hay un cliente que tenga _
    un gran hist�rico de food se tendr�a que marcar como enviada toda esa informaci�n _
    vieja manualmente para que no se la vaya a traer en la proxima corrida del agente, _
    sino que solo suba la informaci�n nueva de manera gradual y constante.
    
    TransferirVentasFOOD                                As Boolean ' Cabecero, Impuestos y Datos Fiscales
    TransferirVentasFOOD_Detalles                       As Boolean ' Detalle, DetalleImp, Pagos
    TransferirVentasFOOD_Vuelto                         As Boolean ' Detalle de Vuelto
    
    POSRetail_SincronizarDevolucionesMultiLocalidad     As Boolean
    
    TransferirAvancesDeEfectivo                         As Boolean
    TransferirCierresPOS                                As Boolean
    
    ReintentarArchivosNoProcesados                      As Boolean
    ReintentarArchivosNoProcesados_ResetLog             As Boolean
    
    Origen_ValidarDocumentoFiscal                       As Integer
    Origen_ValidarDocumentoFiscal_MinutosMax            As Long
    Origen_PlantillaCarExtConvertirProductoPadre        As Dictionary
    
End Type

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public DebugMode                                        As Boolean
'Public DebugOnlyTransferirClientesWebCentral            As Boolean

Public mInterfaz                                        As New cls_Agente_Trans
Public ConexOrigen                                      As New ADODB.Connection
Public ConexDestino                                     As New ADODB.Connection

Global Configuracion                                    As Estruc_Config

Global Const CampoCorrelativoAgente                     As String = "cs_Numero_Transferencia"
Global TxtInfoBajada                                    As String
Global CantArchivosBajada                               As Long

Global TmpArchivoEnProceso                              As String

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Private mClsTmp As Object

Public Const Proc_CopyTable_Limpiar = _
"IF (OBJECT_ID ('CopyTable#', 'P') IS NOT NULL)" & vbNewLine & _
vbTab & "DROP PROCEDURE CopyTable#"

Public Const Proc_CopyTable_Dependencia1 = _
"IF TYPE_ID(N'TestTableType') IS NULL" & vbNewLine & _
vbTab & "CREATE Type TestTableType AS TABLE (ObjectID INT)"

Public Const Proc_CopyTable_Parte1 = _
"CREATE PROCEDURE [dbo].[CopyTable#]" & vbNewLine & _
vbTab & "@DBName SYSNAME" & vbNewLine & _
vbTab & ",@Schema SYSNAME" & vbNewLine & _
vbTab & ",@TableName SYSNAME" & vbNewLine & _
vbTab & ",@IncludeConstraints BIT = 1" & vbNewLine & _
vbTab & ",@IncludeIndexes BIT = 1" & vbNewLine & _
vbTab & ",@NewTableSchema SYSNAME" & vbNewLine & _
vbTab & ",@NewTableName SYSNAME = NULL" & vbNewLine & _
vbTab & ",@NewDBName SYSNAME = NULL" & vbNewLine & _
vbTab & ",@UseSystemDataTypes BIT = 0" & vbNewLine & _
vbTab & ",@CreateTableAndCopyData BIT = 0" & vbNewLine & _
vbTab & ",@Script NVARCHAR(MAX) OUTPUT" & vbNewLine & _
"AS" & vbNewLine & _
"BEGIN" & vbNewLine & _
vbNewLine

Public Const Proc_CopyTable_Parte2 = _
vbTab & "TRY" & vbNewLine & _
vbNewLine & _
vbTab & "IF (@NewDBName IS NULL) SET @NewDBName = @DBName" & vbNewLine & _
vbNewLine & _
vbTab & "IF NOT EXISTS (SELECT * FROM SYS.Types WHERE Name = 'TestTableType')" & vbNewLine & _
vbTab & vbTab & "CREATE TYPE TestTableType AS Table (ObjectID INT)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @SQL NVARCHAR(MAX)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @MainDefinition TABLE (FieldValue VARCHAR(200))" & vbNewLine & _
vbTab & "--DECLARE @DBName SYSNAME" & vbNewLine & _
vbTab & "DECLARE @ClusteredPK BIT" & vbNewLine & _
vbTab & "DECLARE @TableSchema NVARCHAR(255)" & vbNewLine & _
vbNewLine & _
vbTab & "--SET @DBName = DB_NAME(DB_ID())" & vbNewLine

Public Const Proc_CopyTable_Parte3 = _
vbTab & "SELECT @TableName = Name FROM SysObjects WHERE ID = OBJECT_ID(@TableName)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @ShowFields TABLE (FieldID INT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",DatabaseName VARCHAR(100)" & vbNewLine & _
vbTab & ",TableOwner VARCHAR(100)" & vbNewLine & _
vbTab & ",TableName VARCHAR(100)" & vbNewLine & _
vbTab & ",FieldName VARCHAR(100)" & vbNewLine & _
vbTab & ",ColumnPosition INT" & vbNewLine & _
vbTab & ",ColumnDefaultValue VARCHAR(100)" & vbNewLine & _
vbTab & ",ColumnDefaultName VARCHAR(100)" & vbNewLine & _
vbTab & ",IsNullable BIT" & vbNewLine & _
vbTab & ",DataType VARCHAR(100)" & vbNewLine & _
vbTab & ",MaxLength varchar(10)" & vbNewLine & _
vbTab & ",NumericPrecision INT" & vbNewLine & _
vbTab & ",NumericScale INT" & vbNewLine & _
vbTab & ",DomainName VARCHAR(100)" & vbNewLine & _
vbTab & ",FieldListingName VARCHAR(110)" & vbNewLine & _
vbTab & ",FieldDefinition CHAR(1)" & vbNewLine & _
vbTab & ",IdentityColumn BIT" & vbNewLine & _
vbTab & ",IdentitySeed INT" & vbNewLine & _
vbTab & ",IdentityIncrement INT" & vbNewLine & _
vbTab & ",IsCharColumn BIT" & vbNewLine & _
vbTab & ",IsComputed varchar(255))" & vbNewLine

Public Const Proc_CopyTable_Parte4 = _
vbNewLine & _
vbTab & "DECLARE @HoldingArea TABLE(FldID SMALLINT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",Flds VARCHAR(4000)" & vbNewLine & _
vbTab & ",FldValue CHAR(1) DEFAULT(0))" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @PKObjectID TABLE(ObjectID INT)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @Uniques TABLE(ObjectID INT)" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @HoldingAreaValues TABLE(FldID SMALLINT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",Flds VARCHAR(4000)" & vbNewLine & _
vbTab & ",FldValue CHAR(1) DEFAULT(0))" & vbNewLine & _
vbNewLine & _
vbTab & "DECLARE @Definition TABLE(DefinitionID SMALLINT IDENTITY(1,1)" & vbNewLine & _
vbTab & ",FieldValue VARCHAR(200))" & vbNewLine & _
vbNewLine

Public Const Proc_CopyTable_Parte5 = _
vbTab & "SET @SQL=" & vbNewLine & _
vbTab & "'" & vbNewLine & _
vbTab & "USE ['+@DBName+']" & vbNewLine & _
vbTab & "SELECT DISTINCT DB_NAME()" & vbNewLine & _
vbTab & vbTab & ",TABLE_SCHEMA" & vbNewLine & _
vbTab & vbTab & ",TABLE_NAME" & vbNewLine & _
vbTab & vbTab & ",''[''+COLUMN_NAME+'']'' AS COLUMN_NAME" & vbNewLine & _
vbTab & vbTab & ",CAST(ORDINAL_POSITION AS INT)" & vbNewLine & _
vbTab & vbTab & ",COLUMN_DEFAULT" & vbNewLine & _
vbTab & vbTab & ",dobj.name AS ColumnDefaultName" & vbNewLine & _
vbTab & vbTab & ",CASE WHEN c.IS_NULLABLE = ''YES'' THEN 1 ELSE 0 END" & vbNewLine & _
vbTab & vbTab & ",DATA_TYPE" & vbNewLine & _
vbTab & vbTab & ",case CHARACTER_MAXIMUM_LENGTH when -1 then ''max'' else CAST(CHARACTER_MAXIMUM_LENGTH AS varchar) end--CAST(CHARACTER_MAXIMUM_LENGTH AS INT)" & vbNewLine & _
vbTab & vbTab & ",CAST(NUMERIC_PRECISION AS INT)" & vbNewLine & _
vbTab & vbTab & ",CAST(NUMERIC_SCALE AS INT)" & vbNewLine & _
vbTab & vbTab & ",DOMAIN_NAME" & vbNewLine & _
vbTab & vbTab & ",COLUMN_NAME + '',''" & vbNewLine & _
vbTab & vbTab & ",'''' AS FieldDefinition" & vbNewLine & _
vbTab & vbTab & ",CASE WHEN ic.object_id IS NULL THEN 0 ELSE 1 END AS IdentityColumn" & vbNewLine & _
vbTab & vbTab & ",CAST(ISNULL(ic.seed_value,0) AS INT) AS IdentitySeed" & vbNewLine & _
vbTab & vbTab & ",CAST(ISNULL(ic.increment_value,0) AS INT) AS IdentityIncrement" & vbNewLine & _
vbTab & vbTab & ",CASE WHEN st.collation_name IS NOT NULL THEN 1 ELSE 0 END AS IsCharColumn" & vbNewLine

Public Const Proc_CopyTable_Parte6 = _
vbTab & vbTab & ",cc.definition" & vbNewLine & _
vbTab & vbTab & "FROM INFORMATION_SCHEMA.COLUMNS c" & vbNewLine & _
vbTab & vbTab & "JOIN sys.columns sc ON  c.TABLE_NAME = OBJECT_NAME(sc.object_id) AND c.COLUMN_NAME = sc.Name" & vbNewLine & _
vbTab & vbTab & "LEFT JOIN sys.identity_columns ic ON c.TABLE_NAME = OBJECT_NAME(ic.object_id) AND c.COLUMN_NAME = ic.Name" & vbNewLine & _
vbTab & vbTab & "JOIN sys.types st ON COALESCE(c.DOMAIN_NAME,c.DATA_TYPE) = st.name" & vbNewLine & _
vbTab & vbTab & "LEFT OUTER JOIN sys.objects dobj ON dobj.object_id = sc.default_object_id AND dobj.type = ''D''" & vbNewLine & _
vbTab & vbTab & "left join sys.computed_columns cc on c.TABLE_NAME=OBJECT_NAME(cc.object_id) and sc.column_id=cc.column_id" & vbNewLine & _
vbTab & vbTab & "WHERE c.TABLE_NAME = @TableName and c.TABLE_SCHEMA=@schema" & vbNewLine & _
vbTab & vbTab & "ORDER BY c.TABLE_NAME, c.ORDINAL_POSITION" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & "PRINT @SQL" & vbNewLine

Public Const Proc_CopyTable_Parte7 = _
vbTab & "INSERT INTO @ShowFields( DatabaseName" & vbNewLine & _
vbTab & vbTab & ",TableOwner" & vbNewLine & _
vbTab & vbTab & ",TableName" & vbNewLine & _
vbTab & vbTab & ",FieldName" & vbNewLine & _
vbTab & vbTab & ",ColumnPosition" & vbNewLine & _
vbTab & vbTab & ",ColumnDefaultValue" & vbNewLine & _
vbTab & vbTab & ",ColumnDefaultName" & vbNewLine & _
vbTab & vbTab & ",IsNullable" & vbNewLine & _
vbTab & vbTab & ",DataType" & vbNewLine & _
vbTab & vbTab & ",MaxLength" & vbNewLine & _
vbTab & vbTab & ",NumericPrecision" & vbNewLine & _
vbTab & vbTab & ",NumericScale" & vbNewLine & _
vbTab & vbTab & ",DomainName" & vbNewLine & _
vbTab & vbTab & ",FieldListingName" & vbNewLine & _
vbTab & vbTab & ",FieldDefinition" & vbNewLine & _
vbTab & vbTab & ",IdentityColumn" & vbNewLine & _
vbTab & vbTab & ",IdentitySeed" & vbNewLine & _
vbTab & vbTab & ",IdentityIncrement" & vbNewLine & _
vbTab & vbTab & ",IsCharColumn" & vbNewLine & _
vbTab & vbTab & ",IsComputed)" & vbNewLine & _
vbNewLine

Public Const Proc_CopyTable_Parte8 = _
vbTab & "EXEC sp_executesql @SQL," & vbNewLine & _
vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & "SELECT TOP 1 @TableSchema = TableOwner FROM @ShowFields" & vbNewLine & _
vbNewLine & _
vbTab & "INSERT INTO @HoldingArea (Flds) VALUES ('(')" & vbNewLine & _
vbNewLine & _
vbTab & "INSERT INTO @Definition(FieldValue) VALUES ('CREATE TABLE ' + CASE WHEN @NewTableName IS NOT NULL THEN @NewDBName + '.' + @NewTableSchema + '.' + @NewTableName ELSE @NewDBName + '.' + @TableSchema + '.' + @TableName END)" & vbNewLine & _
vbTab & "INSERT INTO @Definition(FieldValue) VALUES ('(')" & vbNewLine & _
vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbNewLine & _
vbTab & "SELECT   CHAR(10) + FieldName + ' ' +" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "CASE WHEN DomainName IS NOT NULL AND @UseSystemDataTypes = 0 THEN DomainName +" & vbNewLine & _
vbTab & vbTab & vbTab & "CASE WHEN IsNullable = 1 THEN ' NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & "Else ' NOT NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & "Else" & vbNewLine

Public Const Proc_CopyTable_Parte9 = _
vbTab & vbTab & vbTab & "case when IsComputed is null then" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "UPPER(DataType) +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN (IsCharColumn = 1 and UPPER(DataType) <> 'TEXT') THEN '(' + CAST(MaxLength AS VARCHAR(10)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "CASE WHEN DataType = 'numeric' THEN '(' + CAST(NumericPrecision AS VARCHAR(10))+','+ CAST(NumericScale AS VARCHAR(10)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "Else" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "CASE WHEN DataType = 'decimal' THEN '(' + CAST(NumericPrecision AS VARCHAR(10))+','+ CAST(NumericScale AS VARCHAR(10)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "Else ''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN IdentityColumn = 1 THEN ' IDENTITY(' + CAST(IdentitySeed AS VARCHAR(5))+ ',' + CAST(IdentityIncrement AS VARCHAR(5)) + ')'" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else ''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN IsNullable = 1 THEN ' NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else ' NOT NULL '" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "CASE WHEN ColumnDefaultName IS NOT NULL AND @IncludeConstraints = 1 THEN 'CONSTRAINT [' + replace(ColumnDefaultName,@TableName,@NewTableName) + '] DEFAULT' + UPPER(ColumnDefaultValue)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "Else ''" & vbNewLine

Public Const Proc_CopyTable_Parte10 = _
vbTab & vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & vbTab & "Else" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "' as '+IsComputed+' '" & vbNewLine & _
vbTab & vbTab & vbTab & "End" & vbNewLine & _
vbTab & vbTab & "END +" & vbNewLine & _
vbTab & vbTab & "CASE WHEN FieldID = (SELECT MAX(FieldID) FROM @ShowFields) THEN ''" & vbNewLine & _
vbTab & vbTab & "Else ','" & vbNewLine & _
vbTab & vbTab & "End" & vbNewLine & _
vbNewLine & _
vbTab & "FROM    @ShowFields" & vbNewLine & _
vbNewLine & _
vbTab & "IF @IncludeConstraints = 1" & vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "SET @SQL =" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & "SELECT  distinct  '',CONSTRAINT ['' + replace(name,@TableName,@NewTableName) + ''] FOREIGN KEY ('' + ParentColumns + '') REFERENCES ['' + ReferencedObject + '']('' + ReferencedColumns + '')''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM ( SELECT   ReferencedObject = OBJECT_NAME(fk.referenced_object_id), ParentObject = OBJECT_NAME(parent_object_id),fk.name" & vbNewLine

Public Const Proc_CopyTable_Parte11 = _
vbTab & vbTab & vbTab & vbTab & ",   REVERSE(SUBSTRING(REVERSE((   SELECT cp.name + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "FROM   sys.foreign_key_columns fkc" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "JOIN sys.columns cp ON fkc.parent_object_id = cp.object_id AND fkc.parent_column_id = cp.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "WHERE fkc.constraint_object_id = fk.object_id   FOR XML PATH('''')   )), 2, 8000)) ParentColumns," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "REVERSE(SUBSTRING(REVERSE((   SELECT cr.name + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "FROM   sys.foreign_key_columns fkc" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "JOIN sys.columns cr ON fkc.referenced_object_id = cr.object_id AND fkc.referenced_column_id = cr.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "WHERE fkc.constraint_object_id = fk.object_id   FOR XML PATH('''')   )), 2, 8000)) ReferencedColumns" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "FROM sys.foreign_keys fk" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "inner join sys.schemas s on fk.schema_id=s.schema_id and s.name=@schema) a" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE ParentObject = @TableName" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & "PRINT @SQL" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema" & vbNewLine

Public Const Proc_CopyTable_Parte12 = vbNewLine & _
vbTab & vbTab & vbTab & "SET @SQL =" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT distinct '',CONSTRAINT ['' + replace(c.name,@TableName,@NewTableName) + ''] CHECK '' + definition" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM sys.check_constraints c join sys.schemas s on c.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE OBJECT_NAME(parent_object_id) = @TableName" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "PRINT @SQL" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "SET @SQL =" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT DISTINCT  PKObject = cco.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM    sys.key_constraints cco" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.index_columns cc ON cco.parent_object_id = cc.object_id AND cco.unique_index_id = cc.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.indexes i ON cc.object_id = i.object_id AND cc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "join sys.schemas s on cco.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE    OBJECT_NAME(parent_object_id) = @TableName    AND  i.type = 1 AND    is_primary_key = 1" & vbNewLine

Public Const Proc_CopyTable_Parte13 = _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "PRINT @SQL" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @PKObjectID(ObjectID)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "SET @SQL=" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT DISTINCT    PKObject = cco.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM    sys.key_constraints cco" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.index_columns cc ON cco.parent_object_id = cc.object_id AND cco.unique_index_id = cc.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.indexes i ON cc.object_id = i.object_id AND cc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "join sys.schemas s on cco.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE    OBJECT_NAME(parent_object_id) = @TableName AND  i.type = 2 AND    is_primary_key = 0 AND    is_unique_constraint = 1" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Uniques(ObjectID)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine

Public Const Proc_CopyTable_Parte14 = _
vbTab & vbTab & vbTab & "SET @ClusteredPK = CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "declare @t TestTableType" & vbNewLine & _
vbTab & vbTab & vbTab & "insert @t select * from @PKObjectID" & vbNewLine & _
vbTab & vbTab & vbTab & "declare @u TestTableType" & vbNewLine & _
vbTab & vbTab & vbTab & "insert @u select * from @Uniques" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "set @sql=" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT distinct '',CONSTRAINT '' + replace(cco.name,@TableName,@NewTableName) + CASE type WHEN ''PK'' THEN '' PRIMARY KEY '' + CASE WHEN pk.ObjectID IS NULL THEN '' NONCLUSTERED '' ELSE '' CLUSTERED '' END  WHEN ''UQ'' THEN '' UNIQUE '' END + CASE WHEN u.ObjectID IS NOT NULL THEN '' NONCLUSTERED '' ELSE '''' END" & vbNewLine & _
vbTab & vbTab & vbTab & "+ ''(''+REVERSE(SUBSTRING(REVERSE(( SELECT   c.name +  + CASE WHEN cc.is_descending_key = 1 THEN '' DESC'' ELSE '' ASC'' END + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM   sys.key_constraints ccok" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN sys.index_columns cc ON ccok.parent_object_id = cc.object_id AND cco.unique_index_id = cc.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN sys.columns c ON cc.object_id = c.object_id AND cc.column_id = c.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN sys.indexes i ON cc.object_id = i.object_id AND cc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "Where i.object_id = ccok.parent_object_id And ccok.object_id = cco.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "order by key_ordinal FOR XML PATH(''''))), 2, 8000)) + '')''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM sys.key_constraints cco" & vbNewLine & _
vbTab & vbTab & vbTab & "inner join sys.schemas s on cco.schema_id=s.schema_id and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "LEFT JOIN @U u ON cco.object_id = u.objectID" & vbNewLine

Public Const Proc_CopyTable_Parte15 = _
vbTab & vbTab & vbTab & "LEFT JOIN @t pk ON cco.object_id = pk.ObjectID" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE    OBJECT_NAME(cco.parent_object_id) = @TableName" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50),@t TestTableType readonly,@u TestTableType readonly'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema,@t=@t,@u=@u" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "End" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "INSERT INTO @Definition(FieldValue) VALUES (')')" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "set @sql=" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & "select '' on '' + d.name + ''([''+c.name+''])''" & vbNewLine & _
vbTab & vbTab & "from sys.tables t join sys.indexes i on(i.object_id = t.object_id and i.index_id < 2)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.index_columns ic on(ic.partition_ordinal > 0 and ic.index_id = i.index_id and ic.object_id = t.object_id)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.columns c on(c.object_id = ic.object_id and c.column_id = ic.column_id)" & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.schemas s on t.schema_id=s.schema_id" & vbNewLine

Public Const Proc_CopyTable_Parte16 = _
vbTab & vbTab & vbTab & vbTab & vbTab & "join sys.data_spaces d on i.data_space_id=d.data_space_id" & vbNewLine & _
vbTab & vbTab & "where t.name=@TableName and s.name=@schema" & vbNewLine & _
vbTab & vbTab & "order by key_ordinal" & vbNewLine & _
vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@schema varchar(50)'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@schema=@schema" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "IF @IncludeIndexes = 1" & vbNewLine & _
vbTab & vbTab & "BEGIN" & vbNewLine & _
vbTab & vbTab & vbTab & "set @sql=" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbTab & vbTab & vbTab & "USE '+@DBName+'" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT distinct '' CREATE '' + i.type_desc + '' INDEX ['' + replace(i.name COLLATE SQL_Latin1_General_CP1_CI_AS,@TableName,@NewTableName) + ''] ON '+@NewDBName+'.'+@NewTableSchema+'.'+@NewTableName+' (''" & vbNewLine & _
vbTab & vbTab & vbTab & "+   REVERSE(SUBSTRING(REVERSE((   SELECT name + CASE WHEN sc.is_descending_key = 1 THEN '' DESC'' ELSE '' ASC'' END + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM  sys.index_columns sc" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.columns c ON sc.object_id = c.object_id AND sc.column_id = c.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE  t.name=@TableName AND  sc.object_id = i.object_id AND  sc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "and is_included_column=0" & vbNewLine

Public Const Proc_CopyTable_Parte17 = _
vbTab & vbTab & vbTab & "ORDER BY key_ordinal ASC   FOR XML PATH('''')    )), 2, 8000)) + '')''+" & vbNewLine & _
vbTab & vbTab & vbTab & "ISNULL( '' include (''+REVERSE(SUBSTRING(REVERSE((   SELECT name + '',''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM  sys.index_columns sc" & vbNewLine & _
vbTab & vbTab & vbTab & "JOIN sys.columns c ON sc.object_id = c.object_id AND sc.column_id = c.column_id" & vbNewLine & _
vbTab & vbTab & vbTab & "WHERE  t.name=@TableName AND  sc.object_id = i.object_id AND  sc.index_id = i.index_id" & vbNewLine & _
vbTab & vbTab & vbTab & "and is_included_column=1" & vbNewLine & _
vbTab & vbTab & vbTab & "ORDER BY key_ordinal ASC   FOR XML PATH('''')    )), 2, 8000))+'')'' ,'''')+''''" & vbNewLine & _
vbTab & vbTab & vbTab & "FROM sys.indexes i join sys.tables t on i.object_id=t.object_id" & vbNewLine & _
vbTab & vbTab & vbTab & "join sys.schemas s on t.schema_id=s.schema_id" & vbNewLine & _
vbTab & vbTab & vbTab & "AND CASE WHEN @ClusteredPK = 1 AND is_primary_key = 1 AND i.type = 1 THEN 0 ELSE 1 END = 1   AND is_unique_constraint = 0   AND is_primary_key = 0" & vbNewLine & _
vbTab & vbTab & vbTab & "where t.name=@TableName and s.name=@schema" & vbNewLine & _
vbTab & vbTab & vbTab & "'" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "print @sql" & vbNewLine & _
vbTab & vbTab & vbTab & "INSERT INTO @Definition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "exec sp_executesql @sql," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "N'@TableName varchar(50),@NewTableName varchar(50),@schema varchar(50), @ClusteredPK bit'," & vbNewLine & _
vbTab & vbTab & vbTab & vbTab & vbTab & "@TableName=@TableName,@NewTableName=@NewTableName,@schema=@schema,@ClusteredPK=@ClusteredPK" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & "End" & vbNewLine

Public Const Proc_CopyTable_Parte18 = _
vbTab & vbTab & vbTab & "INSERT INTO @MainDefinition(FieldValue)" & vbNewLine & _
vbTab & vbTab & vbTab & "SELECT FieldValue FROM @Definition" & vbNewLine & _
vbTab & vbTab & vbTab & "ORDER BY DefinitionID ASC" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "----------------------------------" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "declare @q  varchar(max)" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "set @q=(select replace((SELECT FieldValue FROM @MainDefinition FOR XML PATH('')),'</FieldValue>',''))" & vbNewLine & _
vbNewLine & _
vbTab & vbTab & vbTab & "set @script=(select REPLACE(@q,'<FieldValue>',''))" & vbNewLine & _
vbNewLine & _
"if @CreateTableAndCopyData =1" & vbNewLine & _
"BEGIN" & vbNewLine & _
vbTab & "print  @script" & vbNewLine & _
vbTab & "exec sp_executesql @script" & vbNewLine & _
vbNewLine & _
vbTab & "SELECT Stuff(( SELECT ', '+FieldName FROM @ShowFields FOR XML PATH('') ), 1, 2, '')" & vbNewLine & _
vbNewLine & _
vbTab & "declare @s nvarchar(max)" & vbNewLine & _
vbTab & "set @s=" & vbNewLine & _
vbTab & "'SET IDENTITY_INSERT ' + @NewTableName + ' ON" & vbNewLine & _
vbTab & "Insert ' + @NewTableName + ' ( ' +" & vbNewLine & _
vbTab & "(SELECT Stuff(( SELECT ', '+FieldName FROM @ShowFields FOR XML PATH('') ), 1, 2, ''))" & vbNewLine

Public Const Proc_CopyTable_Parte19 = _
vbTab & "+ ')" & vbNewLine & _
vbTab & "SELECT * from ' + @TableName + '" & vbNewLine & _
vbTab & "SET IDENTITY_INSERT ' + @NewTableName + ' OFF '" & vbNewLine & _
vbTab & "print  @s" & vbNewLine & _
vbTab & "exec sp_executesql @s" & vbNewLine & _
"End" & vbNewLine & _
"END try" & vbNewLine & _
"-- ##############################################################################################################################################################################" & vbNewLine & _
"BEGIN CATCH" & vbNewLine & _
vbTab & "BEGIN" & vbNewLine & _
vbTab & vbTab & vbTab & "Print '***********************************************************************************************************************************************************'" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorNumber               : ' + CAST(ERROR_NUMBER() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorSeverity             : ' + CAST(ERROR_SEVERITY() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorState                : ' + CAST(ERROR_STATE() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorLine                 : ' + CAST(ERROR_LINE() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print 'ErrorMessage              : ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX))" & vbNewLine & _
vbTab & vbTab & vbTab & "Print '***********************************************************************************************************************************************************'" & vbNewLine & _
vbTab & "End" & vbNewLine & _
vbTab & "SET @Script=''" & vbNewLine & _
"END CATCH" & vbNewLine

Public Const Proc_CopyTable_Base = Proc_CopyTable_Parte1 & Proc_CopyTable_Parte2 & Proc_CopyTable_Parte3 _
& Proc_CopyTable_Parte4 & Proc_CopyTable_Parte5 & Proc_CopyTable_Parte6 & Proc_CopyTable_Parte7 _
& Proc_CopyTable_Parte8 & Proc_CopyTable_Parte9 & Proc_CopyTable_Parte10 & Proc_CopyTable_Parte11 _
& Proc_CopyTable_Parte12 & Proc_CopyTable_Parte13 & Proc_CopyTable_Parte14 & Proc_CopyTable_Parte15 _
& Proc_CopyTable_Parte16 & Proc_CopyTable_Parte17 & Proc_CopyTable_Parte18 & Proc_CopyTable_Parte19

Public Function sGetIni(ByVal sIniFile As String, ByVal sSection As String, _
ByVal sKey As String, ByVal sDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim sTemp As String * ParamMaxLength
    Dim nLength As Integer
    
    sTemp = Space$(ParamMaxLength)
    
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, ParamMaxLength, sIniFile)
    
    sGetIni = Left$(sTemp, nLength)
    
End Function

Public Sub sWriteIni(ByVal sIniFile As String, ByVal sSection As String, _
ByVal sKey As String, ByVal sData As String)
    WritePrivateProfileString sSection, sKey, sData, sIniFile
End Sub

Sub Main()
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    If App.PrevInstance Then
        End
    End If
    
    On Error GoTo Err_Descripcion
    
    If IniciarProceso Then
        Call mInterfaz.IniciarAgenteTransferencia(ConexOrigen, ConexDestino)
    End If
    
    End
    
    Exit Sub
    
Err_Descripcion:
    
    ProcesarError "Main", "", "", "", Err.Description, Err.Number, vbCritical
    
End Sub

Private Function IniciarProceso() As Boolean
    
    Dim mValorTemp As String
    Dim mArrTmp As Variant
    
    With Configuracion
        
        .ArchivoInicio = App.Path & "\Configuracion.ini"
        .MostrarDebug = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "MostrarMsg", "")) = 1 _
        Or Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "DebugMode", "")) = 1 _
        Or UCase(Command()) Like UCase("*DebugMode*")
        DebugMode = .MostrarDebug
        .Loc_Origen = sGetIni(.ArchivoInicio, "CONFIGURACION", "Localidad_Origen", "")
        .SoloClientes = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "SoloClientes", "")) = 1
        
        ' Transformamos InterfazCxP en Integer en vez de Boolean por si en el futuro se necesita _
        alguna opci�n adicional o escenario loco.
        .EsInterfazCxP = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "InterfazCxP", ""))
        If .EsInterfazCxP < 0 Then .EsInterfazCxP = 0
        If .EsInterfazCxP > 1 Then .EsInterfazCxP = 1
        
        .EsInterfazCxP_PorLocalidad = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "InterfazCxP_PorLocalidad", "")) = 1
        .EsInterfazCxC = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "InterfazCxC", "")) = 1
        .InterfazCxC_Cruces = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "InterfazCxC_Cruces", "")) = 1 ' Destino.
        .InterfazCxC_NoCancelarOrigen = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "InterfazCxC_NoCancelarOrigen", "")) = 1 ' Origen
        .InterfazCxC_NoSubirAnticiposPendientes = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "InterfazCxC_NoSubirAnticiposPendientes", "0")) = 1 ' Origen
        
        'DebugOnlyTransferirClientesWebCentral = True
        
        .ReintentarArchivosNoProcesados = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ReintentarArchivosNoProcesados", "")) = 1
        .ReintentarArchivosNoProcesados_ResetLog = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ReintentarArchivosNoProcesados_ResetLog", "")) = 1
        .TransferirClientesWebCentral = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "SyncClientesWeb", "")) = 1
        .TransferirDireccionesWebCentral = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "SyncClientesWeb_Direcciones", "")) = 1
        
        .EnviarSolicitudesTransferencia = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "SyncSolicitudTransferencia", "1")) = 1
        .CompletarOrigenSolicitudTransferencia = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "CompletarSolicitudTransferencia", "0")) = 1
        
        .EnviarSolicitudesDeCompra = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "SyncSolicitudCompra", "0")) = 1
        .CompletarOrigenSolicitudCompra = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "CompletarSolicitudCompra", "0")) = 1
        
        .TransferirDetallesPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "TransferirDetallesPOSRetail", "")) = 1
        .TransferirDonacionesPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "TransferirDonacionesPOSRetail", "")) = 1
        .TransferirDetalleVueltoPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "TransferirDetalleVueltoPOSRetail", "")) = 1
        .TransferirDetallePromoPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "TransferirDetallePromoPOSRetail", "")) = 1
        
        .TransferirAvancesDeEfectivo = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "TransferirAvancesDeEfectivo", "")) = 1
        .TransferirCierresPOS = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "TransferirCierresPOS", "")) = 1
        
        .TransferirPendXEntregaPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "TransferirPendXEntregaPOSRetail", "")) = 1
        .TransferirSerialesTransaccionPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "TransferirSerialesTransaccionPOSRetail", "")) = 1
        .TransferirCuponesTransaccionPOSRetail = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "TransferirCuponesTransaccionPOSRetail", "")) = 1
        
        .TransferirVentasFOOD = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "TransferirVentasFOOD", "")) = 1
        .TransferirVentasFOOD_Detalles = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "TransferirVentasFOOD_Detalles", "")) = 1
        .TransferirVentasFOOD_Vuelto = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "TransferirVentasFOOD_Vuelto", "")) = 1
        
        .POSRetail_SincronizarDevolucionesMultiLocalidad = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", _
        "POSRetail_SincronizarDevolucionesMultiLocalidad", "")) = 1
        
        .Origen_ValidarDocumentoFiscal = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ValidarDocumentoFiscal", ""))
        .Origen_ValidarDocumentoFiscal_MinutosMax = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ValidarDocumentoFiscal_MinutosMax", ""))
        
        mArrTmp = sGetIni(.ArchivoInicio, "CONFIGURACION", "Origen_PlantillaCarExtConvertirProductoPadre", Empty)
        
        If Trim(mArrTmp) <> Empty Then
            
            mArrTmp = Split(mArrTmp, "|", , vbTextCompare)
            
            mValorTemp = Empty
            
            For i = 0 To UBound(mArrTmp)
                mValorTemp = mValorTemp & "|" & mArrTmp(i) & ":" & i
            Next
            
            mValorTemp = Mid(mValorTemp, 2)
            
            Set .Origen_PlantillaCarExtConvertirProductoPadre = _
            ConvertirCadenaDeAsociacionAvanzado(mValorTemp, pKeysCaseOption:=2)
            
        End If
        
        .ManejaClienteFrecuente = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "ManejaClienteFrecuente", "")) = 1
        
        .Srv_Origen = sGetIni(.ArchivoInicio, "BD_ORIGEN", "SERVIDOR_ORIGEN", "")
        .BD_Origen = sGetIni(.ArchivoInicio, "BD_ORIGEN", "BASE_DATOS", "VAD10")
        .BD_POS_Origen = sGetIni(.ArchivoInicio, "BD_ORIGEN", "BASE_DATOS_POS", "VAD20")
        
        .User_Origen = sGetIni(.ArchivoInicio, "BD_ORIGEN", "USER_ORIGEN", "SA")
        .Pwd_Origen = sGetIni(.ArchivoInicio, "BD_ORIGEN", "PWD_ORIGEN", "")
        
        If Not (UCase(.User_Origen) = "SA" And Len(.Pwd_Origen) = 0) Then
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .User_Origen = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .User_Origen)
                .Pwd_Origen = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .Pwd_Origen)
            End If
            
        End If
        
        .Srv_Destino = sGetIni(.ArchivoInicio, "BD_DESTINO", "SERVIDOR_DESTINO", "")
        .BD_Destino = sGetIni(.ArchivoInicio, "BD_DESTINO", "BASE_DATOS", "VAD10")
        .BD_POS_Destino = sGetIni(.ArchivoInicio, "BD_DESTINO", "BASE_DATOS_POS", "VAD20")
        
        .User_Destino = sGetIni(.ArchivoInicio, "BD_DESTINO", "USER_DESTINO", "SA")
        .Pwd_Destino = sGetIni(.ArchivoInicio, "BD_DESTINO", "PWD_DESTINO", "")
        
        If Not (UCase(.User_Destino) = "SA" And Len(.Pwd_Destino) = 0) Then
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .User_Destino = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .User_Destino)
                .Pwd_Destino = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .Pwd_Destino)
            End If
            
        End If
        
        .RutaHistoricos = App.Path & "\Archivos_Procesados\"
        .RutaNoProcesados = App.Path & "\Archivos_No_Procesados\"
        .RutaCorrelativosValidos = App.Path & "\Correlativos_Validos\"
        .RutaDestino = sGetIni(.ArchivoInicio, "CONFIGURACION", "Ruta_Archivos", "")
        .RutaDestino = Trim(.RutaDestino)
        .RutaDestino = .RutaDestino & IIf(Right(.RutaDestino, 1) <> "\", "\", "")
        
        'Debug.Print .RutaDestino
        .ValorPunto = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "VALORPUNTO", "100"))
        
        TemporalPuntosCampo = sGetIni(Setup, "CONFIGURACION", "PuntosRetailCampoBase", "?")
        
        Select Case UCase(TemporalPuntosCampo)
        
            Case Is = "TOTAL"
        
                .PuntosRetailCampoBase = "n_Total"
        
            Case Is = "SUBTOTAL"
        
                .PuntosRetailCampoBase = "n_Subtotal"
                
            ' LOS SIGUIENTES PROBABLEMENTE NO TENGAN SENTIDO.
            ' MUY OPCIONALES PERO POR SI ACASO.
                
            Case Is = "IMPUESTO"
                
                .PuntosRetailCampoBase = "n_Impuesto"
                
            Case Is = "MONTO_CANCELADO"
                
                .PuntosRetailCampoBase = "n_Cancelado"
                
            Case Is = "CANTIDAD_PRODUCTOS"
                
                .PuntosRetailCampoBase = "n_Productos"
                
            ' SE DEJA POR DEFECTO EL SUBTOTAL EN BASE A LAS ULTIMAS INCIDENCIAS
            ' ENVIADAS POR EL CLIENTE. (GARZON, MERCAFACIL, ETC)
                
            Case Else
                
                .PuntosRetailCampoBase = "n_Subtotal"
                
        End Select
        
        mValorTemp = sGetIni(.ArchivoInicio, "CONFIGURACION", "Interfaz_Origen", "0")
        .EsOrigen = mValorTemp = "1" Or UCase(mValorTemp) = "SI"
        mValorTemp = sGetIni(.ArchivoInicio, "CONFIGURACION", "Interfaz_Destino", "0")
        .EsDestino = mValorTemp = "1" Or UCase(mValorTemp) = "SI"
        
        '.LongitudSucursal = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "LongitudSucursal", "2"))
        .Acumular_X_Rif = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "AcumularPorRif", "0")) = 1
        
        .SucursalNoCompletaRec = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "NoCompletarRecepcion", "0")) = 1
        .SucursalNoCompletaRec_TiempoMaximoMinutos = SDec(sGetIni(.ArchivoInicio, _
        "CONFIGURACION", "NoCompletarRecepcion_TiempoMaximoMinutos", (36 * 60))) ' 36 Horas
        
        .SucursalNoCompletaNdc = Val(sGetIni(.ArchivoInicio, "CONFIGURACION", "NoCompletarNDC", "0")) = 1
        .SucursalNoCompletaNdc_TiempoMaximoMinutos = SDec(sGetIni(.ArchivoInicio, _
        "CONFIGURACION", "NoCompletarNDC_TiempoMaximoMinutos", (36 * 60))) ' 36 Horas
        
        'If .LongitudSucursal = 0 Then .LongitudSucursal = 2
        
        .IdDepoProd = "DEP"
        .IdMaBancos = "BAN"
        .IdMaInvent = "INV"
        .IdMaVentas = "VEN"
        .IdVentaImp = "VNI"
        .IdTrInvent = "TRI"
        .IdTrVentas = "TRV"
        .IdMACompra = "COM"
        .IDTrCompra = "TRC"
        
        .IdMaClientes = "CLI"
        
        .IdMACompraEspera = "CMW"
        .IDTrCompraEspera = "CTW"
        
        .IdDocumentoFiscal = "DFI"
        
        .IdCompraImp = "CMI"
        .IdCxpCompra = "CXP"
        .IdCxpCompraImp = "CXI"
        .IdCxcVenta = "CXC"
        .IdCxcVentaImp = "CCI"
        
        .IdVentasPos = "VPG" ' Ventas POS Cabecero (MA_PAGOS)
        .IdVentasPosImp = "VPI" ' Ventas POS Impuestos (MA_PAGOS_IMPUESTOS)
        .IdVentasPosDFI = "VPF" ' Ventas POS Datos Fiscales (MA_DOCUMENTOS_FISCAL)
        .IdVentasPosDP = "VPP" ' Ventas POS Detalle Pago (MA_DETALLEPAGO)
        .IdVentasPosDTR = "VPD" ' Ventas POS Detalle (MA_TRANSACCION)
        .IdVentasPosDVLT = "VPV" ' Ventas POS Detalle Vuelto (MA_DETALLEPAGO_VUELTO)
        .IdVentasPOS_DN = "VDN" ' Ventas POS Donaciones (MA_DONACIONES)
        .IdVentasPOS_DN_DP = "VDT" ' Ventas POS Donaciones Detalle (MA_DONACIONES_DETPAG)
        .IdVentasPOS_PxE = "P01" ' Ventas POS Pendiente por Entrega
        .IdVentasPOS_PPxE = "P02" ' Ventas POS Plan Pendiente por Entrega
        .IdVentasPOS_Ser = "P03" ' Ventas POS Seriales
        .IdVentasPOS_Cup = "P04" ' Ventas POS Cupones
        .IdVentasPOS_DetPrm = "P05" ' Ventas POS Detalle Promo (MA_TRANSACCION_DETALLE_PROMO)
        .IDVentasPOS_Pend_Ctrl_Dev = "T01" ' Ventas POS TR Pend Control Dev
        
        .IdSolTra_MA = "STM" ' MA_REQUISICIONES
        .IdSolTra_TR = "STR" ' TR_REQUISICIONES
        .IdSolTra_REL = "STL" ' MA_RELACION_SOLICITUD_TRANSFERENCIA
        
        .IdSolCom_MA = "SCM" ' MA_SOLICITUDCOMPRAS
        .IdSolCom_TR = "SCT" ' TR_SOLICITUDCOMPRAS
        
        .IdVentasFOOD = "FVC" ' Ventas FOOD Cabecero (MA_VENTAS_POS)
        .IdVentasFOOD_Imp = "FVI" ' Ventas FOOD Impuestos (MA_VENTAS_POS_IMPUESTOS)
        .IdVentasFOOD_DFI = "FDF" ' Ventas FOOD Datos Fiscales (MA_DOCUMENTOS_FISCAL)
        .IdVentasFOOD_DP = "FDP" ' Ventas FOOD Detalle Pago (TR_VENTAS_POS_PAGOS)
        .IdVentasFOOD_DTR = "FVD" ' Ventas FOOD Detalle (TR_VENTAS_POS)
        .IdVentasFOOD_DetImp = "FDI" ' Ventas FOOD Detalle Impuestos (TR_VENTAS_POS_IMPUESTOS)
        .IdVentasFOOD_DVLT = "FDV" ' Ventas FOOD Detalle Vuelto (TR_VENTAS_POS_PAGOS_VUELTO)
        
        .IdVentasAvanceEfectivo = "AVC" ' Avances de Efectivo (MA_AVANCE_EFECTIVO)
        .IdVentasAvanceEfectivo_DP = "AVD" ' Avances de Efectivo Detalle (MA_AVANCE_EFECTIVO_DETPAG)
        
        .IdCierresPOS_MA = "CR1" ' Cierres POS Cabecero (MA_CIERRES)
        .IdCierresPOS_TR = "CR2" ' Cierres POS Detalle (TR_CIERRES)
        .IdCierresPOS_Fondo_MA = "CR3" ' Cierres POS Fondo de Caja (MA_CIERRES_FONDO)
        '.IdCierresPOS_Fondo_TR = "CR4" ' Cierres POS Fondo de Caja Detalle (TR_CIERRES_FONDO)
        
        If Not .EsDestino And Not .EsOrigen Then
            
            ProcesarError "InicioProceso", Empty, Empty, Empty, _
            "Debe configurar las opciones del origen o " & _
            "destino en el archivo de configuraci�n", 999, vbCritical
            IniciarProceso = False
            
        ElseIf .EsDestino And .EsOrigen Then
            
            IniciarProceso = .Loc_Origen <> Empty _
            And .Srv_Destino <> Empty _
            And .Srv_Origen <> Empty _
            And .BD_Destino <> Empty _
            And .BD_Origen <> Empty
            
        ElseIf .EsDestino Then
            
            IniciarProceso = .Srv_Destino <> Empty _
            And .BD_Destino <> Empty
            
        Else
            
            IniciarProceso = .Srv_Origen <> Empty _
            And .BD_Origen <> Empty _
            And .Loc_Origen <> Empty
            
        End If
        
    End With
    
End Function

Public Function ManejaClienteFrecuente(pCn) As Boolean
    
    Dim mRs As New ADODB.Recordset
    
    mRs.Open "SELECT * FROM MA_REGLASDENEGOCIO WHERE Campo = 'Loc_ManejaClienteFrecuente'", _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        ManejaClienteFrecuente = Val(mRs!Valor) = 1
    End If
    
End Function

Public Function ProcesarError(ByVal pModulo, ByVal pCorrelativo, ByVal pTabla, _
ByVal pId, ByVal pError, ByVal pNumError, ByVal pTipo As VbMsgBoxStyle)
    If Configuracion.MostrarDebug Then
        GrabarErrores pModulo, pCorrelativo, pTabla, pId, pError, pNumError
        MsgBox pError & " (" & pNumError & ")", pTipo
    Else
        GrabarErrores pModulo, pCorrelativo, pTabla, pId, pError, pNumError
    End If
End Function

Private Sub GrabarErrores(ByVal pModulo, ByVal pCorrelativo, ByVal pTabla, _
ByVal pId, ByVal pError, ByVal pNumError)
    
    Dim ErroresAgente As String
    
    ErroresAgente = App.Path & "\LogError.txt"
    
    Open ErroresAgente For Append Access Write As #1
    Print #1, Now & ", " & pModulo & "," & pCorrelativo & ", " & pTabla & "," _
    & vbNewLine & pId & ", " & pError & " (" & pNumError & ")"
    Close #1
    
End Sub

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function CopyTableScript(pCn As ADODB.Connection, ByVal pDBName As String, ByVal pTable As String, _
ByVal pNewTableName As String, Optional ByVal pTargetDBName As String = "", Optional ByVal pTableSchema = "dbo", _
Optional ByVal pNewTableSchema = "dbo", Optional ByVal pIncludeConstraints As Boolean = True, _
Optional ByVal pIncludeIndexes As Boolean = False, Optional ByVal pUseSystemDataTypes As Boolean = False, _
Optional ByVal pCopyData As Boolean = False) As String
    
    On Error GoTo Error
    
    Dim TmpRows, CmdTemp
    
    If pTargetDBName = vbNullString Then pTargetDBName = pDBName
    
    pCn.Execute Proc_CopyTable_Limpiar, TmpRows
    
    pCn.Execute Proc_CopyTable_Dependencia1, TmpRows
    
    pCn.Execute Proc_CopyTable_Base, TmpRows
    
    Set CmdTemp = New ADODB.Command
    Set CmdTemp.ActiveConnection = pCn
    
    CmdTemp.CommandType = adCmdStoredProc
    
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@DBName", adVarChar, adParamInput, 255, pDBName)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@Schema", adVarChar, adParamInput, 255, pTableSchema)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@TableName", adVarChar, adParamInput, 255, pTable)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@IncludeConstraints", adInteger, adParamInput, 1, IIf(pIncludeConstraints, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@IncludeIndexes", adInteger, adParamInput, 1, IIf(pIncludeIndexes, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewTableSchema", adVarChar, adParamInput, 255, pNewTableSchema)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewTableName", adVarChar, adParamInput, 255, pNewTableName)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@NewDBName", adVarChar, adParamInput, 255, pTargetDBName)
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@UseSystemDataTypes", adInteger, adParamInput, 1, IIf(pUseSystemDataTypes, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@CreateTableAndCopyData", adInteger, adParamInput, 1, IIf(pCopyData, 1, 0))
    CmdTemp.Parameters.Append CmdTemp.CreateParameter("@Script", adVarChar, adParamOutput, (2 ^ 16), 0)
    
    CmdTemp.CommandText = "CopyTable#"
    
    CmdTemp.Execute
    
    CopyTableScript = CmdTemp.Parameters("@Script")
    
    Exit Function
    
Error:
    
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

' Imprime un recordset o el Record Actual utilizando el m�todo original de ADODB.

' El Original de ADODB pero incluyendo los headers.
' Es m�s r�pido pero menos ordenado.
' NO Deja el recordset en su posici�n del cursor original:
' Lo lleva al EOF si es el Recordset, o avanza el cursor una posici�n si es solo el Record Actual.
' Por lo cual habr�a que hacer MoveFirst o MovePrevious respectivamente
' Dependiendo del tipo de recorset para poder volver a recorrerlo.

Public Function QuickPrintRecordset(ByVal pRs As ADODB.Recordset, Optional OnlyCurrentRow As Boolean = False) As String
    
    On Error Resume Next
    
    Dim Data As Boolean
    Dim RsPreview As String
    Dim RowData As String
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    For i = 0 To pRs.Fields.Count - 1
        RsPreview = RsPreview + IIf(i = 0, vbNullString, "|") + pRs.Fields(i).Name
    Next i
    
    RsPreview = RsPreview & vbNewLine
    
    If Data Then
        If OnlyCurrentRow Then
            RowData = pRs.GetString(, 1, "|", vbNewLine, "NULL")
        Else
            RowData = pRs.GetString(, , "|", vbNewLine, "NULL")
        End If
    End If
    
    RsPreview = RsPreview & RowData
    
    QuickPrintRecordset = RsPreview
    
End Function

Public Function QuickPrintRecord(ByVal pRs As ADODB.Recordset) As String
    QuickPrintRecord = QuickPrintRecordset(pRs, True)
End Function

' Imprime un Recordset o el Record Actual de manera ordenada. Deja al recordset en su posici�n (Record) Original.

' Parametros:

' 1) pRs: Recordset
' 2) pRowNumAlias: Alias para una columna que enumera los rows. Si no se especifica o OnlyCurrentRow no se imprime
' 3) OnlyCurrentRow: Imprimir solo el record actual o todo.
' 4) EncloseHeaders: Encierra los cabeceros en Corchetes para mayor separaci�n.
' 5) LineBeforeData: Agrega una Linea de separaci�n entre Header y Records.
' 6) pOnValueError: Que se imprime si da un error a la hora de recuperar un campo.
' 7) Uso interno: Dejar siempre el valor default. Es para poder imprimir los Recordsets anidados (Tipo Consulta Shape).

' Recomendaci�n al obtener el String devuelto: Copiar y Pegar en TextPad o un Bloc de Notas sin Line Wrap para analizar.

Public Function PrintRecordset(ByVal pRs As ADODB.Recordset, Optional ByVal pRowNumAlias As String = vbNullString, _
Optional OnlyCurrentRow As Boolean = False, Optional EncloseHeaders As Boolean = True, _
Optional LineBeforeData As Boolean = False, Optional ByVal pOnValueError As String = "Error|N/A", _
Optional NestLevel As Byte = 0) As String
    
    On Error GoTo FuncError
    
    Dim RsPreview As String, bRowNumAlias As Boolean, RowNumAlias As String, RowNumCount As Double
    Dim OriginalRsPosition, TmpRsValue, mColLength() As Double, mColArrIndex, pRsValues As ADODB.Recordset
    Dim Data As Boolean
    
    If OnlyCurrentRow Then pRowNumAlias = vbNullString
    
    bRowNumAlias = (pRowNumAlias <> vbNullString)
    
    Data = Not (pRs.BOF And pRs.EOF)
    
    ReDim mColLength(pRs.Fields.Count - 1 + IIf(bRowNumAlias, 1, 0))
    
    If bRowNumAlias Then
        RowNumAlias = IIf(EncloseHeaders, "[", vbNullString) & CStr(pRowNumAlias) & IIf(EncloseHeaders, "]", vbNullString)
        RowNumCount = 0
        RsPreview = RsPreview & GetTab(NestLevel) & "$(mCol[0])"
        mColLength(0) = Len(RowNumAlias)
    End If
    
    For i = 0 To pRs.Fields.Count - 1
        'RsPreview = RsPreview + pRs.Fields(i).Name + vbTab
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & "$(mCol[" & mColArrIndex & "])"
        mColLength(mColArrIndex) = Len(IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString))
    Next i
    
    If LineBeforeData Then RsPreview = RsPreview & vbNewLine
    
    If Data And Not OnlyCurrentRow Then
        OriginalRsPosition = SafeProp(pRs, "Bookmark", -1)
        If pRs.CursorType <> adOpenForwardOnly Then
            pRs.MoveFirst
        Else
            pRs.Requery
        End If
        If OriginalRsPosition <> -1 Then Set pRsValues = pRs.Clone(adLockReadOnly)
    Else
        Set pRsValues = pRs
    End If
    
    RowNumCount = 0
    
    Do While Not pRs.EOF
        
        'RsPreview = RsPreview & vbNewLine
        'If bRowNumAlias Then RsPreview = RsPreview & Rellenar_SpaceR(RowNumCount, Len(RowNumAlias), " ")
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            If Len(TmpRsValue) > mColLength(0) Then mColLength(0) = Len(TmpRsValue)
            'RsPreview = RsPreview & TmpRsValue
        End If
        
        For i = 0 To pRs.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRs, pRs.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
                        
            If UCase(TypeName(TmpRsValue)) <> UCase("Recordset") Then
                If Len(TmpRsValue) > mColLength(mColArrIndex) Then mColLength(mColArrIndex) = Len(TmpRsValue)
            End If
            'RsPreview = RsPreview & TmpRsValue
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRs.MoveNext
        
    Loop
    
    If bRowNumAlias Then RsPreview = Replace(RsPreview, "$(mCol[0])", Rellenar_SpaceR(RowNumAlias, mColLength(0), " ") & vbTab, , 1)
    
    For i = 0 To pRs.Fields.Count - 1
        mColArrIndex = IIf(bRowNumAlias, i + 1, i)
        RsPreview = Replace(RsPreview, "$(mCol[" & mColArrIndex & "])", Rellenar_SpaceR( _
        IIf(EncloseHeaders, "[", vbNullString) & pRs.Fields(i).Name & IIf(EncloseHeaders, "]", vbNullString), _
        mColLength(mColArrIndex), " ") & vbTab, , 1)
    Next i
    
    If OriginalRsPosition = -1 And Not OnlyCurrentRow Then
        Set pRsValues = pRs
        If Data Then
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    RowNumCount = 0
    
    Do While Not pRsValues.EOF
        
        RsPreview = RsPreview & GetTab(NestLevel) & vbNewLine
        
        If bRowNumAlias Then
            TmpRsValue = RowNumCount
            RsPreview = RsPreview & GetTab(NestLevel) & Rellenar_SpaceR(TmpRsValue, mColLength(0), " ") & vbTab
        End If
        
        For i = 0 To pRsValues.Fields.Count - 1
            TmpRsValue = isDBNull(SafeItem(pRsValues, pRsValues.Fields(i).Name, pOnValueError), "NULL")
            mColArrIndex = IIf(bRowNumAlias, i + 1, i)
            If Not UCase(TypeName(TmpRsValue)) = UCase("Recordset") Then
                RsPreview = RsPreview & IIf(Not bRowNumAlias, GetTab(NestLevel), vbNullString) & Rellenar_SpaceR(TmpRsValue, mColLength(mColArrIndex), " ") & vbTab
            Else
                RsPreview = RsPreview & vbNewLine & PrintRecordset(TmpRsValue, pRowNumAlias, OnlyCurrentRow, EncloseHeaders, LineBeforeData, pOnValueError, NestLevel + 1)
            End If
        Next i
        
        RowNumCount = RowNumCount + 1
        
        If OnlyCurrentRow Then Exit Do
        
        pRsValues.MoveNext
        
    Loop
    
    PrintRecordset = RsPreview
    
Finally:
    
    On Error Resume Next
    
    If Data And Not OnlyCurrentRow Then
        If OriginalRsPosition <> -1 Then
            If pRs.CursorType = adOpenForwardOnly Then
                pRs.Requery
                pRs.Move OriginalRsPosition, 0
            Else
                SafePropAssign pRs, "Bookmark", OriginalRsPosition
            End If
        Else
            If pRsValues.CursorType <> adOpenForwardOnly Then
                pRsValues.MoveFirst
            Else
                pRsValues.Requery
            End If
        End If
    End If
    
    Exit Function
    
FuncError:
    
    'Resume ' Debug
    
    PrintRecordset = vbNullString
    
    Resume Finally
    
End Function

' Imprime el Record Actual de un Recordset.

Public Function PrintRecord(ByVal pRs As ADODB.Recordset, _
Optional EncloseHeaders As Boolean = True, Optional LineBeforeData As Boolean = False, _
Optional ByVal pOnValueError As String = "Error|N/A") As String
    PrintRecord = PrintRecordset(pRs, , True, EncloseHeaders, LineBeforeData, pOnValueError)
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), car)
End Function

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

' Copia en el portapapeles y Devuelve el string.

Public Function CtrlC(ByVal Text): On Error Resume Next: CtrlC = Text: Clipboard.Clear: Clipboard.SetText CtrlC: End Function

' Devuelve lo que haya en el portapapeles.

Public Function CtrlV() As String: On Error Resume Next: CtrlV = Clipboard.GetText: End Function

Public Function FechaBD(ByVal Expression, _
Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function PathExists(ByVal pPath As String) As Boolean
    On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then
        If Err.Number = 0 Then
            PathExists = True
        End If
    End If
End Function

Public Function SaveTextFile(ByVal pContents As String, ByVal pFilePath As String, _
Optional pUseUnicode As Boolean = False) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(pFilePath, True, pUseUnicode)
    TStream.Write (pContents)
    TStream.Close
    
    SaveTextFile = True

    Exit Function
    
ErrFile:
    
    Debug.Print Err.Description
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

' Changed AS a Pointer to V2.

Public Function Collection_AddKey(pCollection As Collection, ByVal pValor, ByVal pKey As String) As Boolean
    Collection_AddKey = Collection_SafeAddKey(pCollection, pValor, pKey)
End Function

' To attempt to add Item + Key and ignore error in case Key is already contained in the collection.
' Ignore duplication error.

Public Function Collection_SafeAddKey(pCollection As Collection, ByVal pValue, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Add pValue, pKey
    
    Collection_SafeAddKey = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_EncontrarValor(pCollection As Collection, ByVal pValor, Optional ByVal pIndiceInicial As Long = 1) As Long
    Collection_EncontrarValor = Collection_FindValueIndex(pCollection, pValor, pIndiceInicial)
End Function

' How to Use:

' 1) To Know if Value Exists:

'If Collection_FindValueIndex(pCollection, pValue) <> -1 Then

' 2) To Search All Instances (Indexes) of Value in the Collection. Quick Example:

' SearchIndex = Collection_FindValueIndex(pCollection, pValue) ' Start Looking from Index 1

' If SearchIndex <> -1 Then
    ' This should be in a kind of While Loop...
    ' Keep searching other matches starting from next position:
    'SearchIndex = Collection_FindValueIndex(pCollection, pValue, SearchIndex + 1)
    ' Until Search Index = -1
' Else
    ' Next Match not found...
' End if

Public Function Collection_FindValueIndex(pCollection As Collection, ByVal pValue, Optional ByVal pStartIndex As Long = 1) As Long
    
    On Error GoTo Error
    
    Dim i As Long
    
    Collection_FindValueIndex = -1
    
    For i = pStartIndex To pCollection.Count
        If SafeEquals(pCollection.Item(i), pValue) Then
            Collection_FindValueIndex = i
            Exit Function
        End If
    Next i
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteKey(pCollection As Collection, ByVal pKey As String, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteKey = Collection_HasKey(pCollection, pKey)
End Function

' Collection_ExisteKey V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pKey, Null)) Then ' Key might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pKey) ' If this doesn't Proc Error Key is Null, but exists.
        Collection_HasKey = True
    Else ' Key Exists AND <> NULL
        Collection_HasKey = True
    End If
    
    Exit Function
        
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasKey = False ' Only on error, Key is not contained in the Collection.
    
End Function

' Changed AS a Pointer to V2.

Public Function Collection_ExisteIndex(pCollection As Collection, ByVal pIndex As Long, Optional ByVal pItemsAreObjects As Boolean = False) As Boolean
    Collection_ExisteIndex = Collection_HasIndex(pCollection, pIndex)
End Function

' Collection_ExisteIndex V2 (Better) (pItemsAreObjects Knowledge is not required)

Public Function Collection_HasIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    Dim TmpVal
    
    If IsNull(SafeItem(pCollection, pIndex, Null)) Then ' Index might exist but be Null so we do another check.
        SafeVarAssign TmpVal, pCollection.Item(pIndex) ' If this doesn't Proc Error Index also exists.
        Collection_HasIndex = True
    Else ' Index Exists AND <> NULL
        Collection_HasIndex = True
    End If
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    Collection_HasIndex = False ' Only on error Index is not contained in the Collection.
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean
    Collection_RemoveKey = Collection_SafeRemoveKey(pCollection, pKey)
End Function

' Ignore error even if the Key is not contained in the Collection.

Public Function Collection_SafeRemoveKey(pCollection As Collection, ByVal pKey As String) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pKey
        
    Collection_SafeRemoveKey = True
     
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveKey = False ' Non - existant.
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    Collection_RemoveIndex = Collection_SafeRemoveIndex(pCollection, pIndex)
End Function

' Ignore error even if the Index is not contained in the Collection.

Public Function Collection_SafeRemoveIndex(pCollection As Collection, pIndex As Long) As Boolean
    
    On Error GoTo Error
    
    pCollection.Remove pIndex
    
    Collection_SafeRemoveIndex = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Collection_SafeRemoveIndex = False ' Non - existant.
    
End Function

Public Function ConvertirCadenaDeAsociacion(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Collection
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacion = New Collection
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), ":", 2)               'Izq:Clave        'Der:Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        Call Collection_SafeAddKey(ConvertirCadenaDeAsociacion, ParClaveValor(1), ParClaveValor(0))
        
    Next
    
    If ConvertirCadenaDeAsociacion.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacion = Nothing
    
End Function

Public Function ConvertirCadenaDeAsociacionAvanzado(ByVal pCadena As String, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":", _
Optional ByVal pKeysCaseOption As Integer = 0, _
Optional ByVal pValuesCaseOption As Integer = 0) As Dictionary
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    Set ConvertirCadenaDeAsociacionAvanzado = New Dictionary
    
    Dim Item, ParClaveValor
    
    For Each Item In AsEnumerable(Split(pCadena, pSeparador))
        
        ParClaveValor = Split(CStr(Item), pSeparadorInterno, 2) ' Izq -> Clave ' Der -> Valor
        
        If pKeysCaseOption = 1 Then ParClaveValor(0) = LCase(ParClaveValor(0))
        If pKeysCaseOption = 2 Then ParClaveValor(0) = UCase(ParClaveValor(0))
        If pKeysCaseOption = 3 Then ParClaveValor(0) = StrConv(ParClaveValor(0), vbProperCase)
        
        If pValuesCaseOption = 1 Then ParClaveValor(1) = LCase(ParClaveValor(1))
        If pValuesCaseOption = 2 Then ParClaveValor(1) = UCase(ParClaveValor(1))
        If pValuesCaseOption = 3 Then ParClaveValor(1) = StrConv(ParClaveValor(1), vbProperCase)
        
        If Not ConvertirCadenaDeAsociacionAvanzado.Exists(ParClaveValor(0)) Then
            ConvertirCadenaDeAsociacionAvanzado.Add ParClaveValor(0), ParClaveValor(1)
        End If
        
    Next
    
    If ConvertirCadenaDeAsociacionAvanzado.Count = 0 Then Err.Raise 1, , "Empty"
    
    Exit Function
    
Error:
    
    Set ConvertirCadenaDeAsociacionAvanzado = Nothing
    
End Function

Public Function ExportarCadenaDeAsociacion(ByVal pCollection As Collection, _
Optional ByVal pSeparador As String = "|") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacion = Empty
    
    Dim Item
    
    If pCollection.Count <= 0 Then Exit Function
    
    For Each Item In pCollection
        ExportarCadenaDeAsociacion = ExportarCadenaDeAsociacion & _
        pSeparador & Item
    Next
    
    ExportarCadenaDeAsociacion = Mid(ExportarCadenaDeAsociacion, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacion = Empty
    
End Function

Public Function ExportarCadenaDeAsociacionAvanzado(ByVal pValues As Dictionary, _
Optional ByVal pSeparador As String = "|", _
Optional ByVal pSeparadorInterno As String = ":") As String
    
    ' Alternativas comunes para Separador :   ";" , ","
    ' Alternativas comunes para Separador Interno:  ":", "="
    
    On Error GoTo Error
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
    Dim ItemKey, KeysCollection
    
    Set KeysCollection = AsEnumerable(pValues.Keys())
    
    If KeysCollection.Count <= 0 Then Exit Function
    
    For Each ItemKey In KeysCollection
        ExportarCadenaDeAsociacionAvanzado = ExportarCadenaDeAsociacionAvanzado & _
        pSeparador & ItemKey & pSeparadorInterno & pValues.Item(ItemKey)
    Next
    
    ExportarCadenaDeAsociacionAvanzado = Mid(ExportarCadenaDeAsociacionAvanzado, 2)
    
    Exit Function
    
Error:
    
    ExportarCadenaDeAsociacionAvanzado = Empty
    
End Function

Public Function PathExistsV2(pPath As String, _
Optional ByVal pDirectory As Boolean = False) As Boolean
    
    On Error GoTo Error
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    
    'If DebugMode Then Mensaje True, "PathExistsV2: " & pPath
    
    'If pDirectory Then
        'If Dir(pPath, vbDirectory) <> Empty Then PathExistsV2 = True
        PathExistsV2 = GetDirectoryExists(pPath)
    'Else
        If Not PathExistsV2 Then
            'If Dir(pPath) <> Empty Then PathExistsV2 = True
            PathExistsV2 = GetFileExists(pPath)
        End If
    'End If
    
    'If DebugMode Then Mensaje True, "PathExistsV2 (" & pPath & "): " & PathExistsV2
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    If DebugMode Then
        'Mensaje True, "PathExists() " & mErrorDesc & "(" & mErrorNumber & ")"
    End If
    
End Function

Public Function GetFileExists(ByVal pFile As String) As Boolean
    
    On Error GoTo Error
    
    Static FSO As Object
    
    If FSO Is Nothing Then Set FSO = CreateObject("Scripting.FileSystemObject")
    GetFileExists = FSO.FileExists(pFile)
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    If DebugMode Then
        'Mensaje True, "GetFileExists() " & mErrorDesc & "(" & mErrorNumber & ")"
    End If
    
End Function

Public Function GetDirectoryExists(ByVal pDir As String) As Boolean
    
    On Error GoTo Error
    
    Static FSO As Object
    
    If FSO Is Nothing Then Set FSO = CreateObject("Scripting.FileSystemObject")
    GetDirectoryExists = FSO.FolderExists(pDir)
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    If DebugMode Then
        'Mensaje True, "GetDirectoryExists() " & mErrorDesc & "(" & mErrorNumber & ")"
    End If
    
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim POS As Long
    
    POS = InStr(1, pPath, "\")
    
    If POS <> 0 Then
        GetDirectoryRoot = Left(pPath, POS - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim POS As Long
    
    POS = InStrRev(pPath, "\")
    
    If POS <> 0 Then
        GetDirParent = Left(pPath, POS - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExistsV2(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExistsV2(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExistsV2(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExistsV2(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExistsV2(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExistsV2(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExistsV2(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "") As Boolean
    ' pCn > Variant / Object / ADODB.Connection
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTablaV3 = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

Public Function FixTSQL(ByVal pCadena As String) As String
    FixTSQL = Replace(pCadena, "'", "''", , , vbTextCompare)
End Function

' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

' Min Decimal()
Public Function MinDec(ByVal pExpression As Variant, _
Optional ByVal pMinDec As Variant = 0) As Variant
    MinDec = SDec(pExpression, pMinDec)
    MinDec = IIf(MinDec < pMinDec, pMinDec, MinDec)
End Function

' Max Decimal()
Public Function MaxDec(ByVal pExpression As Variant, _
Optional ByVal pMaxDec As Variant = 0) As Variant
    MaxDec = SDec(pExpression, MaxDec)
    MaxDec = IIf(MaxDec > pMaxDec, pMaxDec, MaxDec)
End Function

' Min Decimal > 0
Public Function MinDec0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 1) As Variant
    MinDec0 = SDec(pExpression, pDefaultValue)
    MinDec0 = IIf(MinDec0 > 0, MinDec0, pDefaultValue)
End Function

Public Function SBool(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = False) As Boolean
    On Error Resume Next
    ' Todo n�mero (Int, Double, Decimal) <> es True, 0 es False
    ' Todo String "1", "True", "true" es True, "0", "False", "false" es False
    ' Todo String no num�rico o pExpression que no se pueda convertir a Boolean da Error.
    ' En este caso se retorna pDefaultValue
    SBool = CBool(pExpression)
    If Err.Number <> 0 Then
        SBool = pDefaultValue
    End If
End Function

Public Function RoundDownFive(ByVal pNumber As Variant, _
Optional ByVal pMaxDec As Integer = 8) As Variant
    
    Dim X0, X1, X2, X3, X4, X5, X6, X7, X8, X9, _
    X10, X11, X12, X13, X14, X15, X16
    
    X0 = Round(pNumber - Fix(pNumber), 8)
    
    If X0 <> 0 Then
        
        X1 = Right(CDec(pNumber), 1)
        
        If X1 = "5" Then
            
            X2 = CDec(Round((CDec(pNumber) - Fix(CDec(pNumber))), 8))
            
            X3 = 0
            
            For X4 = 1 To pMaxDec
                
                X5 = (5 / (10 ^ X4))
                
                X6 = X2 / X5
                
                X7 = Fix(X6)
                
                X8 = Round(X6 - X7, 8)
                
                If X8 = 0 Then
                    
                    X3 = X6
                    Exit For
                    
                End If
                
            Next X4
            
            X9 = (1 / X5 / 2)
            
            If (X2 > 0 And X2 < 1) And X3 > 0 And X9 >= 1 Then
                
                For X10 = 0 To pMaxDec
                    
                    If X10 = pMaxDec Or X9 = (10 ^ X10) Then
                        Exit For
                    End If
                    
                Next
                
                If X10 < pMaxDec Then
                    
                    X11 = 1 / (10 ^ (X10 + 1)) * (5 - 1)
                    
                    X12 = 0
                    
                    For X13 = 0 To (pMaxDec - X10 - 1 - 1)
                        X12 = X12 + (9 * (10 ^ X13))
                    Next X13
                    
                    X14 = (X12 / (10 ^ pMaxDec))
                    X15 = Round(X11 + X14, pMaxDec)
                    
                    X16 = ((X6 - 1) * X5)
                    
                    pNumber = Round(CDec(Fix(pNumber) + X16 + X15), pMaxDec)
                    
                End If
                
            End If
            
        End If
        
    End If
    
    RoundDownFive = pNumber
    
End Function
